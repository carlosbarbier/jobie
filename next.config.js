const path = require("path");
const withAntdLess = require('next-plugin-antd-less');
module.exports = withAntdLess({
    reactStrictMode: true,
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')]
    },
    modifyVars: {
        '@primary-color': '#0055d4',
        '@border-radius-base': '0.25em'
    },
    lessVarsFilePathAppendToEndOfContent: false,
    cssLoaderOptions: {},
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: 'http://parttimejobstage-env.eba-5epqzwiu.us-east-1.elasticbeanstalk.com:path*'
            }

        ]
    },
    future: {
        webpack5: true
    },
    eslint: {
        ignoreDuringBuilds: true
    },
    images: {
        domains: ['part-time-api-users-bk.s3.amazonaws.com']
    }
});
