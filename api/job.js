import {getCookie} from "../utils";
import {JOB_URL} from "../constants";


export const saveJob = async (job) => {
    try {
        const response = await fetch(`${JOB_URL}/jobs`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
            body: JSON.stringify(job),
        });
        return await response;
    } catch (error) {

    }
};

export const getJobs = async () => {
    try {
        return await fetch(`${JOB_URL}/jobs`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            }
        });
    } catch (error) {

    }
};

export const applyForJob = async (job_id) => {
    try {
        const response = await fetch(`${JOB_URL}/users/jobs/${job_id}/apply`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
        });
        return await response;
    } catch (error) {

    }
};

export const getMyApplications = async (token) => {
    try {
        const response = await fetch(`${JOB_URL}/users/my/applications`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
        });
        return await response;
    } catch (error) {

    }
};

export const getEmployerJobs= async (token) => {
    try {
        const response = await fetch(`${JOB_URL}/employers/jobs`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
        });
        return await response;
    } catch (error) {

    }
};