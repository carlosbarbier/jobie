import {getCookie, removeCookie, removeFromLocalstorage} from "../utils";
import {AUTH_URL} from "../constants";
import axios from "axios";


export const registerUser = async (user) => {
    try {
        const response = await fetch(`${AUTH_URL}/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response;
    } catch (error) {

    }
};

export const updateEmployerProfile = async (employer) => {
    try {
        const response = await fetch(`${AUTH_URL}/employer/profile`, {
            headers: {
                Accept: "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "PUT",
            body: employer,
        });
        return await response;
    } catch (error) {

    }
};

export const registerCompany = async (company) => {
    try {
        const response = await fetch(`${AUTH_URL}/employer/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(company),
        });
        return await response;
    } catch (error) {

    }
};

export const signInUser = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/login`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;
    } catch (error) {

    }
};

export const logout = async (next) => {
    try {
        await fetch(`${AUTH_URL}/logout`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
        });
        removeCookie("access_token");
        removeCookie("expires_in");
        removeCookie("role");
        removeFromLocalstorage("user");
        removeFromLocalstorage("role");
        removeFromLocalstorage("access_token");
        removeFromLocalstorage("expires_in");
        removeFromLocalstorage("refresh_token");
        next()

    } catch (error) {

    }
};


export const getUser = async (token) => {
    try{
        const response = await fetch(`${AUTH_URL}/me`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            },
        })
        return await response;
    } catch (e) {

    }

};
export const confirmEmailAccount = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/email/confirmation`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};
export const forgotPassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/forgot`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const resendCode = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/resend/code`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const changePassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/change`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const resetPassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/reset`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};