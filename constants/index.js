export const AUTH_URL = "http://parttimejobstage-env.eba-5epqzwiu.us-east-1.elasticbeanstalk.com/api/users"
export const JOB_URL = "http://parttimejobstage-env.eba-5epqzwiu.us-east-1.elasticbeanstalk.com/api"
export const CATEGORIES = [
    {
        "id": "cat100",
        "name": "Childcare"
    },
    {
        "id": "cat1007",
        "name": "Catering"
    },
    {
        "id": "cat104",
        "name": "Pubs, Bars & Clubs"
    },
    {
        "id": "cat103",
        "name": "Hair and Beauty"
    },
    {
        "id": "cat102",
        "name": "Chef Jobs"
    },
    {
        "id": "cat101",
        "name": "Restaurants"
    }
]
export const TYPES = [
    {
        "name": "Permanent"
    },
    {
        "name": "Temporary"
    }
]
export const Role = Object.freeze({
    EMPLOYER: 'employer',
    USER: 'user',
})