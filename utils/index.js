import cookie from "js-cookie";


export const getFromLocalstorage = (key) => {
    if (process.browser) {
        return JSON.parse(localStorage.getItem(key));
    }
};
export const storeToLocalstorage = (key, value) => {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};
export const removeFromLocalstorage = (key) => {
    if (process.browser) {
        localStorage.removeItem(key);
    }
};

export const setCookie = (key, value) => {
    if (process.browser) {
        cookie.set(key, value,);
    }
};

export const removeCookie = (key) => {
    if (process.browser) {
        cookie.remove(key);
    }
};
export const isAuthenticated = (data, next) => {
    setCookie("access_token", data.access_token);
    setCookie("role", data.role);
    setCookie("expires_in", data.expires_in);
    storeToLocalstorage("access_token", data.access_token)
    storeToLocalstorage("expires_in", data.expires_in)
    storeToLocalstorage("refresh_token", data.refresh_token)
    storeToLocalstorage("role", data.role)
    next();
};

export const getCookie = (key, req) => {
    return process.browser ? getCookieFromBrowser(key) : getCookieFromServer(key, req);
};

export const getCookieFromBrowser = key => {
    return cookie.get(key);
};

export const getCookieFromServer = (key, req) => {
    if (req !== undefined) {
        if (!req.headers.cookie) {
            return undefined;
        }

        let token = req.headers.cookie.split(';').find(c => c.trim().startsWith(`${key}=`));
        if (!token) {
            return {};
        }
        return token.split('=')[1];
    }

};

export const getUser = () => {
    const cookieChecked = getCookie("access_token");
    return !!cookieChecked;

};


export const isLogin = () => {
    return getUser();
};

export const getErrors = async (response) => {
    let errors = []
    const json = await response.json();
    if (response.status === 422 || response.status === 403) {
        Object.entries(json.message)
            .forEach(([key, value]) => errors.push(value));
    } else {
        errors.push(json.message)
    }

    return errors;
}
