import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {userConfirmationSchema} from "../validation";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import {confirmEmailAccount} from "../api/auth";
import {getErrors, getFromLocalstorage, removeFromLocalstorage} from "../utils";
import Router from "next/router";
import Notification from "../components/common/Notification";
import LoadingButton from "../components/common/LoadingButton";

const Confirmation = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const [loading, setLoading] = useState(false)
    const {
        register,
        handleSubmit,
        reset,
        formState: {errors}
    } = useForm({resolver: yupResolver(userConfirmationSchema)});
    const [show, setShow] = useState(false)
    const onSubmit = async ({code}) => {
        setLoading(true)
        const email = getFromLocalstorage("new_account_email")
        const response = await confirmEmailAccount({confirm_code: parseInt(code), email: email});
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({code: ""})
            removeFromLocalstorage("new_account_email")
            setShow(true)
            setTimeout(() => {
                Router.push("/login")
            }, 3000)
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 mb-4 py-4 ">
                            <div className="card-body p-6 ">
                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Account Confirmation</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {show && <Notification type="success"
                                                           message="Account Verified"/>}

                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Confirmation Code</label>
                                        <input type="text"
                                               className="form-control"  {...register("code")} />
                                        <span className="text-danger">{errors.code?.message}</span>
                                    </div>
                                    <div>
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Confirm" load_message="Confirming ..."/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Confirmation;