import React, {useState} from 'react';
import {useForm} from "react-hook-form";
import Router from "next/router";
import MainLayout from "../components/layouts/MainLayout";

import {userRegisterSchema} from "../validation";
import {registerCompany} from "../api/auth";
import {getErrors, storeToLocalstorage} from "../utils";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import LoadingButton from "../components/common/LoadingButton";
import Notification from "../components/common/Notification";

const CompanyRegister = () => {
    const [loading, setLoading] = useState(false)
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(userRegisterSchema)});
    const [show, setShow] = useState(false)
    const onSubmit = async (company) => {
        setLoading(true)
        const response = await registerCompany(company);
        if (response.status !== 201) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            storeToLocalstorage("new_account_email",company.email)
            reset({name: "", password: "", email: ""})
            setShow(true)
            setTimeout(() => {
                Router.push("/confirmation")
            }, 3000)
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column  my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 ">
                            <div className="card-body p-6">

                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Company Registration</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)} className="pb-4">
                                    {show && <Notification type="success"
                                                           message="Your account has been created successfully,please check your email to validate your account"/>}

                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">Company Name</label>
                                        <input type="text"
                                               className="form-control"  {...register("name")}/>
                                        <span className="text-danger">{errors.name?.message}</span>
                                    </div>

                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Work Email</label>
                                        <input type="email"
                                               className="form-control" {...register("email")} />
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password</label>
                                        <input type="password" className="form-control"
                                               name="password" {...register("password")}  />
                                        <span className="text-danger">{errors.password?.message}</span>
                                    </div>
                                    <div>
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Register" load_message="Registering"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default CompanyRegister;