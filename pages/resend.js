import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {emailSchema} from "../validation";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import {getErrors} from "../utils";
import Router from "next/router";
import {resendCode} from "../api/auth";
import LoadingButton from "../components/common/LoadingButton";
import Notification from "../components/common/Notification";


const ResendCode = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(emailSchema)});
    const [loading, setLoading] = useState(false)
    const [show, setShow] = useState(false)
    const onSubmit = async ({ email}) => {
        setLoading(true)
        const response = await resendCode({ email: email});
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({email: ""})
            setShow(true)
            setTimeout(() => {
                Router.push("/reset")
            }, 3000)
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 mb-4 ">
                            <div className="card-body p-6 py-5">
                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Resend Confirmation Code</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    {show && <Notification type="success"
                                                           message="Check your email for the verification code"/>}
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control"  {...register("email")}/>
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>
                                    <div>
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Confirm" load_message="Confirming ..."/>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default ResendCode;