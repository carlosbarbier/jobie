import MainLayout from "../components/layouts/MainLayout";
import Link from "next/link";

export default function Home() {
    const description = `Our mission is to help you to find a job easily based on your schedule
                            `
    return (
        <MainLayout>
               <div id='hero' className="section">
                   <div className="section-center">
                       <div className="container">
                           <div className="row">
                               <div className="col-md-7 mx-auto col-md-push-3">
                                   <div className="hero-cta">
                                       <h1>Jobie,<br/>
                                           Browse and Find
                                           <br/>
                                           <span className="text-primary">The perfect job</span>
                                       </h1>
                                       <p style={{fontSize: "37px", lineHeight: "1.1"}} className="mt-3">
                                           {description}
                                           <span className="fw-bold fs-1 text-primary">.</span>
                                       </p>
                                   </div>
                                   <div className="mt-4">
                                       <Link href="/jobs">
                                           <a className="btn btn-lg btn-primary cta-btn"  style={{width:"150px"}}>Browse</a>
                                       </Link>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>

        </MainLayout>
    )
}
