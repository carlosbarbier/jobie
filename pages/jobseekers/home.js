import React from 'react';
import JobSeekerLayout from "../../components/layouts/JobseekerLayout";
import MyApplicationComponent from "../../components/jobseeker/MyApplicationComponent";
import {getCookie} from "../../utils";
import {getMyApplications} from "../../api/job";

const Home = ({applications}) => {

    return (
        <JobSeekerLayout>
            <MyApplicationComponent applications={applications}/>
        </JobSeekerLayout>
    );
};
Home.getInitialProps = async (ctx) => {
    const token = getCookie('access_token', ctx.req);
    let applications = null;
    if (token) {
        try {
            const response = await getMyApplications(token)
            applications = await response.json()
        } catch (error) {
            if (error.response.status !== 200) {
                applications = null;
            }
        }
    }
    if (applications === null) {
        ctx.res.writeHead(302, {Location: '/login'});
        ctx.res.end();
    } else {

        return {applications: applications}
    }

}
export default Home;