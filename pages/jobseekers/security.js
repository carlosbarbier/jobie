import React from 'react';
import JobSeekerLayout from "../../components/layouts/JobseekerLayout";
import SecurityComponent from "../../components/employer/SecurityComponent";

const Security = () => {
    return (
        <JobSeekerLayout>
            <SecurityComponent/>
        </JobSeekerLayout>
    );
};

export default Security;