import React, {useEffect, useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {applyForJob, getJobs} from "../api/job";
import {getCookieFromBrowser, getFromLocalstorage, storeToLocalstorage} from "../utils";
import Image from "next/image";
import {getUser} from "../api/auth";
import Notification from "../components/common/Notification";

const Jobs = ({jobs}) => {
    const [job_applied, setJobs] = useState([]);
    const [loading, setLoading] = useState(false)
    const [login, setIsLogin] = useState(true)
    const [success, setSuccess] = useState(false)
    const [job_id, setJobId] = useState("")
    const [message, setMessage] = useState("User session expired or user account does not exist. Please login to your account or create a new account to apply.")
    const [savedId, setSaveId] = useState("")

    useEffect(() => {
        const found = getFromLocalstorage('jobs')
        if (found !== null) {
            setJobs(found)
        }
    }, [job_id,success]);

    async function handleApply(e) {
        let id = e.target.attributes.getNamedItem('data-id').value
        setJobId(id)
        setLoading(true)
        const token = getCookieFromBrowser('access_token')
        const user = await getUser(token)
        if (user.status === 401) {
            setLoading(false)
            setIsLogin(false)
        } else if (user.status === 200) {
            setIsLogin(true)
            const response = await applyForJob(id);
            if (response.status !== 200) {
                setLoading(false)
            } else {
                setLoading(false)
                setSuccess(true)
                const arr = []
                const jobs = getFromLocalstorage("jobs")
                if (jobs === undefined || jobs === null) {
                    arr.push(id)
                    storeToLocalstorage("jobs", arr)
                } else {
                    setSaveId(id)
                    const toStore = [...jobs, id]
                    storeToLocalstorage("jobs", toStore)
                }
            }
        } else {
            setLoading(false)
            setMessage('Unexpected error occurred. Please try later')
        }

    }

    return (
        <MainLayout>
            <div className="container mt-5">
                <div className="d-flex justify-content-between">
                    <h4>All Available Jobs</h4>
                </div>
                <div className="row my-2 g-3">
                    {!login && <Notification type="alert alert-danger" message={message}/>}
                    {success && <Notification type="alert alert-success" message="Job application saved successfully."/>}
                    {jobs && jobs.map(job => (
                        <div className="col-md-3" key={job.id}>
                            <div className="card p-2 border-0">
                                <div className="text-right text-capitalize"><small>{job.type}</small></div>
                                <div className="text-center mt-3 p-3">
                                    <Image src={job.employer.image} alt="image" width="60" height="60"
                                           className="rounded"/>
                                    <span
                                        className="d-block font-weight-bold">{job.title}</span>
                                    <hr/>
                                    <span>{job.employer.name}</span>
                                    <div className="d-flex flex-row align-items-center justify-content-center">
                                        <small className="ml-1 text-uppercase">{job.address}</small>
                                    </div>
                                    <div className="d-flex justify-content-between mt-3">
                                        <span>{job.hours_per_weeks}h/week</span>
                                        {((job_applied && job_applied.includes(job.id)) || job.id===savedId)
                                            ?
                                            <button className="btn btn-sm btn-outline-dark" disabled={true}
                                                    style={{minWidth: "90px"}}>
                                                Applied
                                            </button>
                                            :
                                            <button className="btn btn-sm btn-outline-dark" data-id={job.id} style={{minWidth: "90px"}}
                                                    onClick={handleApply}>
                                                {loading && job.id===job_id
                                                    ? <><span className="spinner-border spinner-border-sm mx-1"
                                                              role="status"
                                                              aria-hidden="true"/> </>
                                                    : "Apply Now"
                                                }
                                            </button>

                                        }

                                    </div>
                                </div>
                            </div>
                        </div>

                    ))}
                </div>
            </div>
        </MainLayout>
    );
};

export async function getStaticProps() {
    const response = await getJobs()
    const jobs = await response.json()

    return {
        props: {
            jobs
        },
        revalidate: 10,
    }
}

export default Jobs;