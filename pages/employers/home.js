import React from 'react';
import EmployerHome from "../../components/employer/EmployerHome";
import EmployerLayout from "../../components/layouts/EmployerLayout";
import {getCookie} from "../../utils";
import {getEmployerJobs} from "../../api/job";

const Home = ({jobs}) => {
    return (
        <EmployerLayout>
            <EmployerHome jobs={jobs}/>
        </EmployerLayout>
    );
};
Home.getInitialProps = async (ctx) => {
    const token = getCookie('access_token', ctx.req);
    let jobs = null;
    if (token) {
        try {
            const response = await getEmployerJobs(token)
            jobs = await response.json()
        } catch (error) {
            if (error.response.status !== 200) {
                jobs = null;
            }
        }
    }
    if (jobs === null) {
        ctx.res.writeHead(302, {Location: '/login'});
        ctx.res.end();
    } else {

        return {jobs: jobs}
    }

}
export default Home;