import React from 'react';
import EmployerLayout from "../../components/layouts/EmployerLayout";
import UpdateProfile from "../../components/employer/UpdateProfile";

const Profile = () => {
    return (
        <EmployerLayout>
            <UpdateProfile/>
        </EmployerLayout>
    );
};

export default Profile;