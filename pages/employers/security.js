import React from 'react';
import EmployerLayout from "../../components/layouts/EmployerLayout";
import SecurityComponent from "../../components/employer/SecurityComponent";

const Security = () => {
    return (
        <EmployerLayout>
            <SecurityComponent/>
        </EmployerLayout>
    );
};

export default Security;