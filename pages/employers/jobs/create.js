import React from 'react';
import EmployerLayout from "../../../components/layouts/EmployerLayout";
import CreateJob from "../../../components/employer/CreateJob";

const Create = () => {
    return (
        <EmployerLayout>
            <CreateJob/>
        </EmployerLayout>
    );
};

export default Create;