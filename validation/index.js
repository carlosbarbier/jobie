import * as Yup from "yup";

export const userRegisterSchema = Yup.object().shape({
    name: Yup.string()
        .required('Name is required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const changePasswordSchema = Yup.object().shape({
    password_confirmation: Yup.string()
        .oneOf([Yup.ref('new_password'), null], 'Passwords must match'),
    current_password: Yup.string()
        .required('Password is required'),
    new_password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const jobSchema = Yup.object().shape({
    title: Yup.string()
        .required('Title is required'),
    description: Yup.string()
        .required('Description is required'),
    address: Yup.string()
        .required('Address is required'),
    city: Yup.string()
        .required('City is required'),
    hours: Yup.string()
        .required('Weekly hours is required')
        .matches(/^\d+/, 'Weekly hours must be number'),
    category: Yup.string()
        .required('Category is required'),
    type: Yup.string()
        .required('Type of job is required'),
});

export const resetPasswordSchema = Yup.object().shape({
    confirm_code: Yup.string()
        .required('Code required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const userLoginSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .required('Password is required'),
});

export const emailSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
});

export const userConfirmationSchema = Yup.object().shape({
    code: Yup.string()
        .required('Confirmation code is required')
        .matches(/^\d+/, 'Code must be a number')
});

export const userForgotPasswordSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid')
});

export const employerProfileSchema = Yup.object().shape({
    address: Yup.string()
        .required('Address is required'),
    phone: Yup.string()
        .required('Phone number is required'),

    image: Yup.mixed()
        .test('required', "Image is required", (value) =>{
            return value && value.length
        } )
        .test("fileSize", "The file is too large", (value) => {
            return value && value[0] && value[0].size <= 200000;
        })
        .test("type", "We only support jpeg/png", function (value) {
            return value && value[0] && (value[0].type === "image/png" || value[0].type === "image/jpeg" );
        }),

});