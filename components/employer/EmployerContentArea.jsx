import React from 'react';

const EmployerContentArea = ({children}) => {
    return (
        <>
            {children}
        </>
    );
};

export default EmployerContentArea;