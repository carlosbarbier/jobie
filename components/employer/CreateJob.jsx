import React, {useState} from 'react';
import ErrorComponent from "../common/ErrosComponent";
import {useForm} from "react-hook-form";

import {jobSchema} from "../../validation";
import {getErrors} from "../../utils";
import {yupResolver} from "@hookform/resolvers/yup";
import LoadingButton from "../common/LoadingButton";
import {CATEGORIES, TYPES} from "../../constants";
import {saveJob} from "../../api/job";
import Notification from "../common/Notification";

const CreateJob = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const [loading, setLoading] = useState(false)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(jobSchema)});
    const [show, setShow] = useState(false)
    const onSubmit = async (data) => {
        setShow(false)
        setLoading(true)
        const payload = {
            title: data.title,
            description: data.description,
            job_type: data.type,
            address: data.address,
            latitude: "default",
            longitude: "default",
            city: data.city,
            hours_per_weeks: data.hours,
            category_id: data.category
        }
        const response = await saveJob(payload);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({title: "", description: "", address: '', city: '', hours: ''})
            setShow(true)
        }
    };
    return (
        <div className="bg-white p-3">
            <div className="container d-flex flex-column">

                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-12">
                        <div className="card-body">
                            {show && <Notification type="success"
                                                   message="Job created successfully"/>}
                            <form onSubmit={handleSubmit(onSubmit)}>
                                {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                <div className="mb-3">
                                    <label htmlFor="title" className="form-label">Title</label>
                                    <input type="text"
                                           className="form-control"  {...register("title")}/>
                                    <span className="text-danger">{errors.title?.message}</span>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="description" className="form-label">Description</label>
                                    <textarea className="form-control" rows="3" {...register("description")}/>
                                    <span className="text-danger">{errors.description?.message}</span>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <label htmlFor="address" className="form-label">Location</label>
                                        <input type="text"
                                               className="form-control"  {...register("address")}/>
                                        <span className="text-danger">{errors.address?.message}</span>
                                    </div>
                                    <div className="col">
                                        <label htmlFor="city" className="form-label">City</label>
                                        <input type="text"
                                               className="form-control"  {...register("city")}/>
                                        <span className="text-danger">{errors.city?.message}</span>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col">
                                        <label htmlFor="type" className="form-label">Type</label>
                                        <select className="form-control" {...register("type")}
                                                style={{textIndent: "4px"}}>
                                            {TYPES && TYPES.map(type => (
                                                <option key={type.name}
                                                        value={type.name}>{type.name}</option>
                                            ))}
                                        </select>
                                        <span className="select-arrow"/>
                                        <span className="text-danger">{errors.type?.message}</span>
                                    </div>

                                    <div className="col">
                                        <label htmlFor="category" className="form-label">Category</label>
                                        <select className="form-control" {...register("category")}
                                                style={{textIndent: "4px"}}>
                                            {CATEGORIES && CATEGORIES.map(category => (
                                                <option key={category.id}
                                                        value={category.id}>{category.name}</option>
                                            ))}
                                        </select>
                                        <span className="select-arrow"/>
                                        <span className="text-danger">{errors.category?.message}</span>
                                    </div>
                                    <div className="col">
                                        <label htmlFor="hours" className="form-label">Hours/Week</label>
                                        <input type="text"
                                               className="form-control"  {...register("hours")}/>
                                        <span className="text-danger">{errors.hours?.message}</span>
                                    </div>
                                </div>
                                <div>
                                    <div className="d-grid pb-3 my-5">
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Create Job" load_message="Creating Job"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default CreateJob;