import React, {useState} from 'react';
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {employerProfileSchema} from "../../validation";
import {updateEmployerProfile} from "../../api/auth";
import {getErrors} from "../../utils";
import ErrorComponent from "../common/ErrosComponent";
import Notification from "../common/Notification";
import LoadingButton from "../common/LoadingButton";

const UpdateProfile = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const [loading, setLoading] = useState(false)
    const {
        register,
        handleSubmit,
        reset,
        formState: {errors}
    } = useForm({resolver: yupResolver(employerProfileSchema)});
    const [show, setShow] = useState(false)

    const onSubmit = async (data) => {
        setShow(false)
        setLoading(true)
        const formData = new FormData();
        formData.append("image", data.image[0]);
        formData.append('address', data.address);
        formData.append('phone', data.phone);

        const response = await updateEmployerProfile(formData);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({image: "", address: '', phone: ''})
            setShow(true)
        }
    };
    return (
        <div className="bg-white p-3">
            <div className="container d-flex flex-column  my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-12">
                        <div className="card-body pb-5">
                            {show && <Notification type="success"
                                                   message="Profile updated successfully"/>}
                            <form onSubmit={handleSubmit(onSubmit)}>
                                {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                <div className="mb-3">
                                    <label htmlFor="address" className="form-label">Address</label>
                                    <input type="text"
                                           className="form-control"  {...register("address")}/>
                                    <span className="text-danger">{errors.address?.message}</span>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="phone" className="form-label">Phone</label>
                                    <input type="text"
                                           className="form-control"  {...register("phone")}/>
                                    <span className="text-danger">{errors.phone?.message}</span>
                                </div>
                                <div className="mb-4">
                                    <label htmlFor="image" className="form-label">Image</label>
                                    <input className="form-control" type="file"  {...register("image")} />
                                    <span className="text-danger">{errors.image?.message}</span>
                                </div>

                                <div className="d-grid pb-3 my-5">
                                    <div className="d-grid ">
                                        <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                       message="Update Profile" load_message="Updating Profile"/>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default UpdateProfile;