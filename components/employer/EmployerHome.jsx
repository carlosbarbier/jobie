import React from 'react';
import moment from "moment";

const EmployerHome = ({jobs}) => {
    console.log(jobs)
    return (
        <div className="card bg-white border-0 rounded-0 dashboard-content py-5 px-5">
            <p className="h4 mb-4">My Jobs</p>
            <table className="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Address</th>
                    <th>Hours</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                {jobs && jobs.map((job, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{job.title}</td>
                        <td>{job.category.name}</td>
                        <td>{job.type}</td>
                        <td>{job.address}</td>
                        <td>{job.hours_per_weeks}</td>
                        <td>{moment(job.created_at).fromNow()}</td>
                    </tr>))}

                </tbody>
            </table>

        </div>
    );
};

export default EmployerHome;