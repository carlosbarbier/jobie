import React from 'react';

import ChangeProfile from "./ChangeProfile";
import ChangePassword from "./ChangePassword";

const SecurityComponent = (user) => {

    return (

        <div className="profile-wrapper">
            <div className="row">
                <div className="col-md-6">
                    <ChangeProfile user={user}/>
                </div>
                <div className="col-md-6">
                    <ChangePassword/>
                </div>
            </div>
        </div>

    );
};

export default SecurityComponent;