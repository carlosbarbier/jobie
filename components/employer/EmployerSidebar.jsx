import React from 'react';
import Link from "next/link";
import {logout} from "../../api/auth";
import Router from "next/router";

const EmployerSidebar = () => {
    return (
        <nav className="navbar navbar-expand-md  mb-4 rounded-3">
            <div className="collapse navbar-collapse">
                <div className="navbar-nav flex-column">

                    <span className="navbar-header fw-bolder ">Jobs</span>
                    <ul className="list-unstyled mb-4">
                        <li className="nav-item">
                            <Link href="/employers/home">
                                <a className="nav-link"> <i className="bi bi-calendar3-event  mr-2"/>{""} My Jobs</a>
                            </Link>

                        </li>
                        <li className="nav-item">
                            <Link href="/employers/jobs/create">
                                <a className="nav-link" href="#"> <i className="bi bi-receipt mr-2"/> Post a new Job</a>
                            </Link>
                        </li>

                    </ul>

                    <span className="navbar-header fw-bolder">Account Settings</span>
                    <ul className="list-unstyled ms-n2 mb-0">
                        <li className="nav-item">
                            <Link href="/employers/security">
                                <a className="nav-link"><i className="bi bi-file-lock mr-2"/> {""}Security</a>
                            </Link>

                        </li>
                        <li className="nav-item">
                            <Link href="/employers/profile">
                                <a className="nav-link" href="#"><i className=" bi bi-gear  mr-2"/>{""} Update profile</a>
                            </Link>

                        </li>
                        <li className="nav-item">
                            <a className="nav-link disabled" href="#"><i className="bi bi-bell mr-2"/>{""} Notifications</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link disabled" href="#"><i className="bi bi-file-earmark-lock mr-2"/>{""} Profile
                                Privacy</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link disabled" href="#"> <i className="bi bi-archive mr-2"/>{""} Delete
                                Profile</a>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link disabled " href="#"> <i className="bi bi-folder-symlink"/> {""} Linked
                                Accounts</a>
                        </li>

                        <li className="nav-item hoverable">
                            <a className="nav-link"
                               onClick={() => logout(() => Router.replace("/login"))}>
                                <i className="bi bi-box-arrow-right"/> Sign Out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    );
};

export default EmployerSidebar;