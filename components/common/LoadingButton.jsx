import React from 'react';

const LoadingButton = ({loading,message,load_message,css_class}) => {
    return (
        <button type="submit" className={css_class}>
            {loading
                ? <><span className="spinner-border spinner-border-sm mx-1"
                          role="status" aria-hidden="true"/> {load_message}</>
                : message
            }
        </button>
    );
};

export default LoadingButton;