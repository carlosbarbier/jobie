import React from 'react';
import {Alert} from 'antd';

const Notification = ({type, message,}) => {

    return (
        <Alert
            style={{paddingTop:"15px",paddingBottom:"15px", marginBottom:"15px"}}
            message={message}
            type={type}
            closable
        />
    )

};

export default Notification;