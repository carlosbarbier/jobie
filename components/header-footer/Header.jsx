import React from 'react';
import Link from "next/link";
import Image from "next/image";
import {getCookieFromBrowser, getUser, isLogin} from "../../utils";
import UserDashboard from "./UserDashboard";



const Header = () => {
    const user = getUser()
    const role =getCookieFromBrowser('role')
    return (
        <div id="header">
            <nav className="navbar navbar-expand-lg mt-2">
                <div className="container">
                    <div className="d-flex flex-grow-1">
                        <Link href="/">
                            <a>
                                <span className="w-100 d-lg-none d-block"/>
                                <Image src="/logo.png" alt="logo" width="150" height="70"/>
                            </a>

                        </Link>

                    </div>
                    <div className="collapse navbar-collapse flex-grow-1 text-right mt-3  me-5 ">
                        <ul className="navbar-nav ms-auto flex-nowrap">
                            <li className="nav-item">
                                <Link href="/jobs"
                                      as={`/jobs`}>
                                    <a className="nav-link m-2 menu-item fw-bold">jobs</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <a href="#" className="nav-link m-2 menu-item fw-bold disabled">Contact us</a>
                            </li>
                            <li className="nav-item">
                                <a href="#" className="nav-link m-2 menu-item fw-bold disabled">Contact us</a>
                            </li>
                            <li className="nav-item">
                                <Link href="/about"
                                      as={`/about`}>
                                    <a className="nav-link m-2 menu-item fw-bold disabled">Career</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    {!isLogin() &&
                        <div className="collapse navbar-collapse authentication mt-3">
                            <div className="ms-auto mt-3 mt-lg-0">
                                <Link href="/login">
                                    <a className="btn btn-white shadow-sm me-1 fw-bold">Sign In</a>
                                </Link>

                                <Link href="/register">
                                    <a className="btn btn-primary fw-bold btn_register">Register</a>
                                </Link>
                            </div>
                        </div>
                    }
                    {user && role==='user' &&  <UserDashboard url="/jobseekers/home"/>}
                    {user && role==='employer' &&  <UserDashboard url="/employers/home"/>}
                </div>
            </nav>
        </div>


    );
};

export default Header;