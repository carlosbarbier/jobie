import React from 'react';
import Link from "next/link";
const Footer = () => {
    return (
        <div className="pt-lg-10 pt-5 footer bg-white">
            <div className="container">
                <div className="row align-items-center g-0 border-top py-2 mt-6">
                    <div className="col-lg-4 col-md-5 col-12">
                        <span>© 2022 Jobie, Inc. All Rights Reserved</span>
                    </div>
                    <div className="col-12 col-md-7 col-lg-8 d-md-flex justify-content-end">
                        <nav className="nav nav-footer">
                            <Link href='/company-register'>
                               <a className="nav-link ps-0">Company Registration</a>
                            </Link>
                            <a className="nav-link ps-0" href="#">Privacy Policy</a>
                            <a className="nav-link px-2 px-md-3" href="#">Cookie Notice </a>
                            <a className="nav-link" href="#">Terms of Use</a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;