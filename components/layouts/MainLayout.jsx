import Header from "../header-footer/Header";
import Footer from "../header-footer/Footer";

const MainLayout = ({children}) => {

    return (
        <>
            <Header/>
            <main>
                {children}
            </main>
            <Footer/>
        </>
    );
};

export default MainLayout;