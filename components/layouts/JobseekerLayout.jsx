import React from 'react';
import Header from "../header-footer/Header";
import Footer from "../header-footer/Footer";
import Link from "next/link";
import JobseekerSidebar from "../jobseeker/JobseekerSidebar";
import JobseekerContentArea from "../jobseeker/JobseekerContentArea";

const JobSeekerLayout = ({children}) => {
    return (
        <>
            <Header/>
            <section id="student-area ">
                <div className="container ">
                    <div className="border-0 mt-3 rounded-3 py-4 teacher-area-header bg-primary">
                        <div className="row ">
                            <div className="offset-lg-1 col-lg-10 col-md-12 col-12">
                                <div className="d-lg-flex align-items-center justify-content-between pb-4">
                                    <div className=" mb-lg-0">
                                        <h2 className="text-white mb-1">Dashboard</h2>
                                    </div>
                                    <div>
                                        <Link href="/">
                                            <a className="btn btn-default " style={{backgroundColor: "#ffffff"}}>Go to
                                                Home</a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container mb-5 mt-2">
                    <div className="row ms-0">
                        <div className="col-3 bg-white rounded-3">
                            <JobseekerSidebar/>
                        </div>
                        <div className="col-9">
                            <JobseekerContentArea>
                                {children}
                            </JobseekerContentArea>
                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </>
    );
};

export default JobSeekerLayout;