import React from 'react';

const JobseekerContentArea = ({children}) => {
    return (
        <>
            {children}
        </>
    );
};

export default JobseekerContentArea;