import React from 'react';
import Image from "next/image";
import moment from "moment";

const MyApplicationComponent = ({applications}) => {
    return (

            <div className="container d-flex flex-column">
                    <div className="row  gx-3">
                        {applications && applications.map(application=>(
                            <div className="col-md-4 mb-3" key={application.id}>
                                <div className="card border-0 p-2">
                                    <div className="text-right"><small>{application.type}</small></div>
                                    <div className="text-center mt-2 p-3">
                                        <Image src={application.employer.image} alt="image" width="60" height="60" className="rounded"/>
                                        <span className="d-block font-weight-bold">{application.title}</span>
                                        <hr/>
                                        <span>{application.employer.name}</span>
                                        <div className="d-flex flex-row align-items-center justify-content-center">
                                            <small className="ml-1">{application.address}</small></div>
                                        <div className="d-flex justify-content-between mt-3">
                                            <div>{application.hours_per_weeks}h/week</div>
                                            <div className="text-muted fw-normal">{moment(application.apply_date).fromNow()}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}

                </div>
            </div>

    );
};

export default MyApplicationComponent;